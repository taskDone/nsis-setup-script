NSIS Setup

----
# 界面
![setup1](http://taskdone.qiniudn.com/nsis_1.png)
![setup2](http://taskdone.qiniudn.com/nsis_2.png)
![setup3](http://taskdone.qiniudn.com/nsis_3.png)
![setup4](http://taskdone.qiniudn.com/nsis_4.png)
# Excel插件NSIS结构

## 安装前环境判断

### 是否已经安装本程序

如果安装，即**升级**
要记录安装位置，安装至此位置

### 是否安装Office2007以上版本

----
**Office**

版本|32位|64位
:-:|:-:|:-:
2007|×|×
2010|√|√
2013|√|√

*如果没有安装Office，则退出*


### Office是否正在运行

### 操作系统：x86/x64

* 注册表操作：x86和x64位置不同
* 依赖的软件：x86和x64不同


### 是否安装msi3.1以上版本

### 是否安装.NET Framework4.0 Full

### 是否安装VSTOR40

### 是否安装Office2007PIARedist

## 安装中配置

### 自定义安装界面

### 如果Office正在运行，关闭

kill

### 下载依赖的软件环境

#### 安装msi

* 32位即可

#### 安装.NET Framework 4.0

* 下载前，判断磁盘空间大小是否满足条件

* ~~下载full版本，安装选项设置x86~~

#### 安装vsto

#### 安装pia

### 下载主程序

### 解压至安装目录（升级的话，解压至上次安装目录

### 设置安装目录everyone权限(CACLS %1 /E /T /C /P everyone:F)

### 注册vsto，xll等

#### vsto、守护vsto注册

##### HKCU

##### HKLM

###### Office2007

###### Office2010

###### Office2013

##### 去除视图保护

#### xll注册

#### URL协议注册

#### 导入安全证书

### 将安装路径写入系统环境变量

HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment

## 安装后的设置

### 向注册表中写入卸载信息

### 生成卸载程序uninstall.exe

## 卸载程序

### 自定义界面

### Office是否运行

### 删除安装目录文件

### 删除注册表信息