Var MSG

Var BGImage
Var MiddleImage
Var ImageHandle
Var Btn_Next
Var Btn_Waring

Var Btn_Agreement
Var Bool_License

Var Btn_Close

Var Ck_ShortCut
Var Bool_ShortCut
Var Lbl_ShortCut

Var Lbl_Sumary
Var PB_ProgressBar
Var WebImg ;网页控件
Var PPercent

Var WarningForm

#XLL OPEN
Var open
Var i
Var Bool_Xll_Installed
  	
Var ISMSI
Var ISNETF

;Office 版本
Var Bool_Office2007
Var Bool_Office2010
Var Bool_Office2013
Var Bool_Office2010_64
Var Bool_Office2013_64
;是否x86
Var Bool_X86
Var Bool_Office_Running
var BOOL_CSFDATA_INSTALLED
Var INSTALLED_DIR

Var BOOL_VSTO
Var BOOL_PIA

!ifndef TARGETDIR
!define TARGETDIR "..\bin"
!endif

!include "MUI2.nsh"
!include "WinCore.nsh"
!include "FileFunc.nsh"
!include "nsWindows.nsh"
!include "nsDialogs.nsh"
!include "WinMessages.nsh"
!include "WordFunc.nsh"
!include "LogicLib.nsh"
!include "StrFunc.nsh"
!include "Registry.nsh"
!include "x64.nsh"
;!include "UsefulLib.nsh"

;------User define
!define THIS_APP "数库Excel插件"
!define THIS_APP_PRODUCT_CODE "{B3442623-C731-421B-9210-0103C955C959}"
!define THIS_VERSION "1.0"
!define THIS_OUT_FILE "ChinaScope.exe"
!define THIS_COMPANY "数库财务咨询有限公司"
!define THIS_URL "http://www.chinascopefinancial.com/"
!define THIS_INSTALL_DIR "C:\ChinaScope\CSFData"
!define THIS_URL_PROTOCOL_NAME "csfdata"
!define THIS_URL_PROTOCOL_EXE_COMMAND '"${THIS_INSTALL_DIR}\csfdata.exe" "%1" "%2" "%3"'
!define LICENSE_URL "http://www.chinascopefinancial.com/website/privacy_policy"
!define EXCEL_APP "EXCEL.EXE"
!define MAX_OPTION_OPEN 15

!define MAIN_Program_URL "http://update.otc-china.cn/updater/plugin/CSFData.zip"

!define MSI_URL "http://download.microsoft.com/download/1/4/7/147ded26-931c-4daf-9095-ec7baf996f46/WindowsInstaller-KB893803-v2-x86.exe"
!define NF_URL "http://download.microsoft.com/download/9/5/A/95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE/dotNetFx40_Full_x86_x64.exe"

!define VSTO40_x86_URL "http://update.otc-china.cn/software/vstor40_x86.exe"
!define VSTO40_x64_URL "http://update.otc-china.cn/software/vstor40_x64.exe"

!define PIA2007_URL "http://update.otc-china.cn/software/o2007pia.msi"
!define PIA2010_URL "http://update.otc-china.cn/software/o2010pia.msi"
!define PIA2007_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\{50120000-1105-0000-0000-0000000FF1CE}"
!define PIA2010_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-1105-0000-0000-0000000FF1CE}"

;------安装 注册表信息--------------------------------
!define INSTALL_PUBLICKEY "<RSAKeyValue><Modulus>2xmRZAlHK6P6HooTrDMPGsClaCrJop1GgBdhG6dU76XeD+2Ur0xQ/JHyuOmhoeV+vHL3vZ5C3CZ0fvUtMBIsUOoY6fNvrFyKFl1eGEgXhAZ5r9SrTWuoA5iPxe4gUTrdpY1u4c1k5U5Gm0tMW3FRET1NcjWmWiTEwmoV7mnEzwc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>"
!define INSTALL_VSTO "ADAExcelPlugin.Ribbon.vsto"
!define INSTALL_VSTO_GUID "318B9D74-1E0C-461A-BC09-0371CC27EC90"
!define INSTALL_WATCH_VSTO "ADAExcelPlugin.RibbonWatch.vsto"
!define INSTALL_WATCH_VSTO_GUID "8B5B23AD-083E-4CBF-B9EB-F40EB6082822"
!define INSTALL_XLL "ADAExcelPlugin.CSFUDF.xll"
!define INSTALL_AddInDesc "CSF Data"
;------End--------------------------------------------

!addplugindir "${TARGETDIR}"

;---------------------------全局编译脚本预定义的常量-----------------------------------------------------
!define PRODUCT_NAME ${THIS_APP}
!define PRODUCT_VERSION ${THIS_VERSION}
!define PRODUCT_PUBLISHER ${THIS_COMPANY}
!define PRODUCT_WEB_SITE ${THIS_URL}
!define EM_BrandingText ${THIS_COMPANY}
!define OUTFILE_NAME ${THIS_OUT_FILE}

!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${THIS_APP_PRODUCT_CODE}"

!define MUI_ABORTWARNING
!define MUI_ICON "images\chinascope.ico"
!define MUI_UNICON "images\chinascope.ico"

!addplugindir "Plugins"

;应用程序显示名字
Name ${THIS_APP}
;应用程序输出路径
OutFile "${OUTFILE_NAME}"
InstallDir ${THIS_INSTALL_DIR}

SetCompressor lzma
BrandingText ${THIS_COMPANY}
SetCompress force
XPStyle on

!define MUI_CUSTOMFUNCTION_GUIINIT onGUIInit
!define MUI_CUSTOMFUNCTION_ABORT onUserAbort

;自定义页面
Page custom WelcomePage
Page custom CompletePage
UninstPage custom un.WelcomePage ; un.CompletePage

; 许可协议页面
!define MUI_LICENSEPAGE_CHECKBOX

; 安装界面包含的语言设置
!insertmacro MUI_LANGUAGE "SimpChinese"
RequestExecutionLevel admin

Section MainSetup
SectionEnd

Section Uninstall
  	Delete "$INSTDIR\uninst.exe"
  	Delete "$INSTDIR\*.*"
  	RMDir /r "$INSTDIR"
  
  	DeleteRegKey HKLM "${PRODUCT_UNINST_KEY}"
    ;WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "CSFData" "${THIS_INSTALL_DIR}"
		DeleteRegValue HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "CSFData"
  	SetAutoClose true
SectionEnd

#========================== 安装 ===============================================
Function .onInit
    InitPluginsDir
    File `/ONAME=$PLUGINSDIR\bg.bmp` `images\bg.bmp`
    File `/ONAME=$PLUGINSDIR\bg_buttom.bmp` `images\bg_buttom.bmp`
    File `/ONAME=$PLUGINSDIR\success.bmp` `images\success.bmp`
    File `/ONAME=$PLUGINSDIR\btn_next.bmp` `images\btn_next.bmp`
    File `/oname=$PLUGINSDIR\btn_agreement.bmp` `images\btn_agreement.bmp`
    File `/oname=$PLUGINSDIR\btn_close.bmp` `images\btn_close.bmp`
    File `/oname=$PLUGINSDIR\checkbox1.bmp` `images\checkbox1.bmp`
    File `/oname=$PLUGINSDIR\checkbox2.bmp` `images\checkbox2.bmp`
    File `/oname=$PLUGINSDIR\waring.bmp` `images\waring.bmp`

    File `/oname=$PLUGINSDIR\loading1.bmp` `images\loading1.bmp`
    File `/oname=$PLUGINSDIR\loading2.bmp` `images\loading2.bmp`

    File `/oname=$PLUGINSDIR\trust.bat` `images\trust.bat`

    SkinBtn::Init "$PLUGINSDIR\btn_next.bmp"
    SkinBtn::Init "$PLUGINSDIR\btn_agreement.bmp"
    SkinBtn::Init "$PLUGINSDIR\btn_close.bmp"
    SkinBtn::Init "$PLUGINSDIR\checkbox1.bmp"
    SkinBtn::Init "$PLUGINSDIR\checkbox2.bmp"
    SkinBtn::Init "$PLUGINSDIR\waring.bmp"
FunctionEnd

Function onGUIInit
    ;消除边框
    System::Call `user32::SetWindowLong(i$HWNDPARENT,i${GWL_STYLE},0x9480084C)i.R0`
    ;隐藏一些既有控件
    GetDlgItem $0 $HWNDPARENT 1034
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1035
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1036
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1037
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1038
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1039
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1256
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1028
    ShowWindow $0 ${SW_HIDE}
FunctionEnd

;处理无边框移动
Function onGUICallback
  ${If} $MSG = ${WM_LBUTTONDOWN}
    SendMessage $HWNDPARENT ${WM_NCLBUTTONDOWN} ${HTCAPTION} $0
  ${EndIf}
FunctionEnd

Function SkinBtn_Next
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_next.bmp $1
FunctionEnd

Function SkinBtn_Waring
  SkinBtn::Set /IMGID=$PLUGINSDIR\waring.bmp $1
FunctionEnd

Function un.SkinBtn_Next
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_uninst.bmp $1
FunctionEnd

Function un.SkinBtn_Waring
  SkinBtn::Set /IMGID=$PLUGINSDIR\waring.bmp $1
FunctionEnd

Function un.SkinBtn_Close
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_close.bmp $1
FunctionEnd

Function SkinBtn_Agreement
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_agreement.bmp $1
FunctionEnd

Function SkinBtn_Close
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_close.bmp $1
FunctionEnd

Function SkinBtn_Quit
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_quit.bmp $1
FunctionEnd

Function SkinBtn_Cancel
  SkinBtn::Set /IMGID=$PLUGINSDIR\btn_cancel.bmp $1
FunctionEnd

Function SkinBtn_Checked
  SkinBtn::Set /IMGID=$PLUGINSDIR\checkbox2.bmp $1
FunctionEnd

Function SkinBtn_UnChecked
  SkinBtn::Set /IMGID=$PLUGINSDIR\checkbox1.bmp $1
FunctionEnd

;=========== 环境Check ===========
Function CheckEnvironments
	;x86/x64
	Call DiskCheck

	;是否已经安装过
	Call InstalledCheck

	;判断是否已经安装了Office,以及Excel是否正在运行
	Call OfficeEnvironmentCheck

	;判断是否安装了MSI
	Call MSIEnvironmentCheck

	;是否安装了.NET Framework 4.0 Full
	Call NFEnvironmentCheck

	;判断是否安装VSTO40
	Call VSTO40EnvironmentCheck

	;判断PIA是否安装
	Call PIA2007EnvironmentCheck

	;显示check结果
	${If} $Bool_X86 == 1
	  StrCpy $0 "系统结构:[x86]"
	${Else}
	  StrCpy $0 "系统结构:[x64]"
	${EndIf}

	${If} $BOOL_CSFDATA_INSTALLED == 1
	  StrCpy $0 "$0$\n是否安装本程序:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装本程序:[未安装]"
	${EndIf}

	${If} $Bool_Office2007 == 1
	  StrCpy $0 "$0$\n是否安装Office2007:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装Office2007:[未安装]"
	${EndIf}

	${If} $Bool_Office2010 == 1
	  StrCpy $0 "$0$\n是否安装Office2010:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装Office2010:[未安装]"
	${EndIf}

	${If} $Bool_Office2010_64 == 1
	  StrCpy $0 "$0 架构:[x64]"
	${Else}
	  StrCpy $0 "$0 架构:[x86]"
	${EndIf}

	${If} $Bool_Office2013 == 1
	  StrCpy $0 "$0$\n是否安装Office2013:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装Office2013:[未安装]"
	${EndIf}

	${If} $Bool_Office2013_64 == 1
	  StrCpy $0 "$0 架构:[x64]"
	${Else}
	  StrCpy $0 "$0 架构:[x86]"
	${EndIf}

	${If} $Bool_Office_Running == 1
	  StrCpy $0 "$0$\nExcel是否正在运行:[是]"
	${Else}
	  StrCpy $0 "$0$\nExcel是否正在运行:[否]"
	${EndIf}

	${If} $ISMSI == 1
	  StrCpy $0 "$0$\n是否安装了MSI:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装了MSI:[未安装]"
	${EndIf}

	${If} $ISNETF == 1
	  StrCpy $0 "$0$\n是否安装了.NET Framework 4.0:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装了.NET Framework 4.0:[未安装]"
	${EndIf}

	${If} $BOOL_VSTO == 1
	  StrCpy $0 "$0$\n是否安装了VSTO40:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装了VSTO40:[未安装]"
	${EndIf}

	${If} $BOOL_PIA == 1
	  StrCpy $0 "$0$\n是否安装了PIA:[已安装]"
	${Else}
	  StrCpy $0 "$0$\n是否安装了PIA:[已安装]"
	${EndIf}

	MessageBox MB_OK $0
FunctionEnd

;--------- 判断当前系统，是否是x86 ---------------------------------------------
Function DiskCheck
	${If} ${RunningX64}
	  StrCpy $Bool_X86 0
	${Else}
	  StrCpy $Bool_X86 1
	${EndIf}
	/*
  ReadRegStr $1 HKLM "Hardware\Description\System\CentralProcessor\0" Identifier
  StrCpy $2 $1 3
	${If} $2 == 'x86'
		StrCpy $Bool_X86 1
	${Else}
	  StrCpy $Bool_X86 0
	${EndIf}
	*/
FunctionEnd

;-------- 判断是否安装过主程序 -------------------------------------------------
Function InstalledCheck
	${If} $Bool_X86 == 1
	  SetRegView 32
	${Else}
	  SetRegView 64
	${EndIf}

	ReadRegStr $R1 HKEY_LOCAL_MACHINE ${PRODUCT_UNINST_KEY} "UninstallString"
	${If} $R1 != ""
	  StrCpy $BOOL_CSFDATA_INSTALLED 1
	  StrCpy $INSTALLED_DIR $R1
	${Else}
	  StrCpy $BOOL_CSFDATA_INSTALLED 0
	${EndIf}
FunctionEnd

;---------  Office -------------------------------------------------------------
Function OfficeEnvironmentCheck
	;office version
	StrCpy $0 "12.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2007

  StrCpy $0 "14.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2010
  Call OfficeBitEnvironmentCheck

  StrCpy $0 "15.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2013
  Call OfficeBitEnvironmentCheck

  ;excel is running?
  Call OfficeIsRunningCheck

FunctionEnd

;检测Office是否安装
;  [in]  $0  officeversion(12.0/14.0/15.0)
;  [out] $1  bool(1:installed)
Function CheckOfficeVersion
  System::Call "*(i 0) i .R0"
	Push "Software\Microsoft\Office\$0"
	system::call 'Advapi32::RegOpenKey(i 0x80000002, t s, i R0) .iR1'
	system::call 'Advapi32::RegCloseKey(i R0)'
	system::free
	${If} $R1 == 0
		StrCpy $1 1
	${ElseIf} $R1 == 2
		StrCpy $1 0
	${Else}
		StrCpy $1 0
	${EndIf}
	Push $1
	;1.判断32位位置上是否有office，如果有，进一步判断Excel.exe是否存在
FunctionEnd

Function OfficeBitEnvironmentCheck
	/*
	${If} $Bool_X86 == 1
	  SetRegView 32
	${Else}
	  SetRegView 64
	${EndIf}
	*/
	;SetRegView 32
	ReadRegStr $R0 HKLM "Software\Microsoft\Office\$0\Common\InstallRoot" "Path"
	StrCmp $R0 "" NotPath 0
  StrCpy $R0 "$R0\EXCEL.EXE"
	IfFileExists $R0 0 NotPath
  MessageBox MB_OK $R0
	FileOpen $0 $R0 r
	FileSeek $0 0x3C
	FileReadByte $0 $R0
	FileReadByte $0 $R1
	IntOp $R1 $R1 << 8
	IntOp $R0 $R0 + 4
	IntOp $R0 $R0 + $R1
	FileSeek $0 $R0
	FileReadByte $0 $R1
	FileReadByte $0 $R2
	IntFmt $R2 "%X" $R2
	IntFmt $R1 "%X" $R1
	MessageBox MB_OK "[$R2$R1]"
	FileClose $0
	NotPath:
FunctionEnd

;Office是否正在运行
Function OfficeIsRunningCheck
  FindProcDLL::FindProc ${EXCEL_APP}
	Sleep 500
	Pop $R0
	${If} $R0 != 0
   	StrCpy $Bool_Office_Running 1
	${Else}
	  StrCpy $Bool_Office_Running 0
	${EndIf}
FunctionEnd

;关闭Office进程
Function CloseExcelApp
/*
		FindProcDLL::FindProc ${EXCEL_APP}
    Sleep 500
    Pop $R0
    ${If} $R0 != 0
    	KillProcDLL::KillProc ${EXCEL_APP}
    ${EndIf}
    */
    FindProcDLL::FindProc ${EXCEL_APP}
		Pop $R0
		StrCmp $R0 1 0  +3
		KillProcDLL::KillProc ${EXCEL_APP}
		Goto -4
FunctionEnd

;---------  MSI ----------------------------------------------------------------
Function MSIEnvironmentCheck
	GetDllVersion "$SYSDIR\msi.dll" $R0 $R1

	IntOp $R2 $R0 >> 16
	IntOp $R2 $R2 & 0x0000FFFF ; $R2 主版本
	IntOp $R3 $R0 & 0x0000FFFF ; $R3 次版本
	IntOp $R4 $R1 >> 16
	IntOp $R4 $R4 & 0x0000FFFF ; $R4
	IntOp $R5 $R1 & 0x0000FFFF ; $R5
	StrCpy $0 "$R2.$R3.$R4.$R5" ; $0 now contains string like "1.2.0.192"
	${VersionCompare} $0 "3.1.4000.2435" $R0
	;MessageBox MB_OK $R0
	${If} $R0 == 2
		StrCpy $ISMSI 0
	${Else}
	  StrCpy $ISMSI 1
	${EndIf}
FunctionEnd

;下载MSI
Function MSIInstallation32
	${NSD_SetText} $Lbl_Sumary "正在下载Microsoft Installer 3.1"
	ShowWindow $PPercent ${SW_SHOW} ;显示正在下载
	${NSD_SetText} $PPercent "0%"

	inetc::get /hwnd $PPercent /hwnd2 $PPercent /probar $PB_ProgressBar /caption " " /popup "" "http://download.microsoft.com/download/1/4/7/147ded26-931c-4daf-9095-ec7baf996f46/WindowsInstaller-KB893803-v2-x86.exe" "$TEMP\WindowsInstaller-KB893803-v2-x86.exe"  /end
	Pop $0
	${If} $0 == "Transfer Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSEIF} $0 == "SendRequest Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSE}
		;刷新界面
		System::Call "user32::InvalidateRect(i $hwndparent,i0,i 1)"
		Call MSIInstall32
	${EndIf}
FunctionEnd

;安装MSI
Function MSIInstall32
	${NSD_SetText} $Lbl_Sumary "正在安装Microsoft Installer 3.1"
	ShowWindow $PPercent ${SW_HIDE}
	nsExec::ExecToStack '"$TEMP\WindowsInstaller-KB893803-v2-x86.exe" /quiet /norestart /overwriteoem"'
	Delete "$TEMP\WindowsInstaller-KB893803-v2-x86.exe"
FunctionEnd

;---------  .NET Framework 4.0 full --------------------------------------------
Function NFEnvironmentCheck
  Call GetNetFrameworkVersion
	Pop $R1
	;DetailPrint $R1
	${If} $R1 < '4.0.30319'
	  StrCpy $ISNETF 0
	${Else}
	  StrCpy $ISNETF 1
	${EndIf}
FunctionEnd

Function NF40Installation
	${NSD_SetText} $Lbl_Sumary "正在下载Microsoft .NET Framework"
	ShowWindow $PPercent ${SW_SHOW} ;显示正在下载
	${NSD_SetText} $PPercent "0%"

  ;NSISdl::download /TRANSLATE2 '正在下载 %s' '正在连接...' '(剩余 1 秒)' '(剩余 1 分钟)' '(剩余 1 小时)' '(剩余 %u 秒)' '(剩余 %u 分钟)' '(剩余 %u 小时)' '已完成：%skB(%d%%) 大小：%skB 速度：%u.%01ukB/s' /TIMEOUT=7500 /NOIEPROXY 'http://download.microsoft.com/download/9/5/A/95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE/dotNetFx40_Full_x86_x64.exe' '$TEMP\dotNetFx40_Full_x86_x64.exe'
	inetc::get /hwnd $PPercent /hwnd2 $PPercent /probar $PB_ProgressBar /caption " " /popup "" "http://download.microsoft.com/download/9/5/A/95A9616B-7A37-4AF6-BC36-D6EA96C8DAAE/dotNetFx40_Full_x86_x64.exe" "$TEMP\dotNetFx40_Full_x86_x64.exe"  /end
	Pop $0
	${If} $0 == "Transfer Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSEIF} $0 == "SendRequest Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSE}
		;刷新界面
		System::Call "user32::InvalidateRect(i $hwndparent,i0,i 1)"
		Call InstallNF
	${EndIf}
FunctionEnd

Function InstallNF
	${NSD_SetText} $Lbl_Sumary "正在安装Microsoft .NET Framework"
	ShowWindow $PPercent ${SW_HIDE}
	nsExec::ExecToStack '"$TEMP\dotNetFx40_Full_x86_x64.exe" /quiet /norestart /overwriteoem"'
	Delete "$TEMP\dotNetFx40_Full_x86_x64.exe"
FunctionEnd

#;check NF Version
Function GetNetFrameworkVersion
	;获取.Net Framework版本支持
	Push $1
	Push $0
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Install"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Version"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5" "Install"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5" "Version"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\Setup" "InstallSuccess"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\Setup" "Version"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727" "Install"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727" "Version"
	StrCmp $1 "" +1 +2
	StrCpy $1 "2.0.50727.832"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322" "Install"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322" "Version"
	StrCmp $1 "" +1 +2
	StrCpy $1 "1.1.4322.573"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	ReadRegDWORD $0 HKLM "SOFTWARE\Microsoft\.NETFramework\policy\v1.0" "Install"
	ReadRegDWORD $1 HKLM "SOFTWARE\Microsoft\.NETFramework\policy\v1.0" "Version"
	StrCmp $1 "" +1 +2
	StrCpy $1 "1.0.3705.0"
	StrCmp $0 1 KnowNetFrameworkVersion +1
	StrCpy $1 "not .NetFramework"
	KnowNetFrameworkVersion:
	Pop $0
	Exch $1
FunctionEnd

;---------  VSTO40 -------------------------------------------------------------
Function VSTO40EnvironmentCheck
	SetRegView 32
	ReadRegDWORD $R1 HKLM "SOFTWARE\Microsoft\VSTO Runtime Setup\v4" "Install"
  ;MessageBox MB_OK "[$R1]"
	${If} $R1 == 1
	  StrCpy $BOOL_VSTO 1
	${Else}
	  StrCpy $BOOL_VSTO 0
	${EndIf}
FunctionEnd

Function VSTO40Install
	${If} ${Bool_X86} == 1
	  StrCpy $R0 ${VSTO40_x86_URL}
	${Else}
	  StrCpy $R0 ${VSTO40_x64_URL}
	${EndIf}

	${NSD_SetText} $Lbl_Sumary "正在下载VSTO40"
	ShowWindow $PPercent ${SW_SHOW} ;显示正在下载
	${NSD_SetText} $PPercent "0%"

	inetc::get /hwnd $PPercent /hwnd2 $PPercent /probar $PB_ProgressBar /caption " " /popup "" $R0 "$TEMP\vstor40.exe"  /end
	Pop $0
	${If} $0 == "Transfer Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSEIF} $0 == "SendRequest Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSE}
		;刷新界面
		System::Call "user32::InvalidateRect(i $hwndparent,i0,i 1)"
		Call InstallNF
	${EndIf}
FunctionEnd

Function VSTOInstall

  ${NSD_SetText} $Lbl_Sumary "正在安装VSTO40"
	ShowWindow $PPercent ${SW_HIDE}
	nsExec::ExecToStack '"$TEMP\vstor40.exe" /quiet /norestart /overwriteoem"'
	Delete "$TEMP\vstor40.exe"
FunctionEnd

;---------  Office2007PIARedist ------------------------------------------------
Function PIA2007EnvironmentCheck
	SetRegView 32

	${If} $Bool_Office2013 == 1
	${OrIf} $Bool_Office2010 == 1
	  StrCpy $R1 ${PIA2010_KEY}
	${ElseIf} $Bool_Office2007 == 1
	  StrCpy $R1 ${PIA2007_KEY}
	${Else}
	  StrCpy $R1 ""
	${EndIf}

	;MessageBox MB_OK $R1

	${If} $R1 != ""
	  ReadRegDWORD $R2 HKLM $R1 "UninstallString"
	  ${If} $R2 != ""
	    StrCpy $BOOL_PIA 1
	  ${Else}
	    StrCpy $BOOL_PIA 0
	  ${EndIf}
	${Else}
	  StrCpy $BOOL_PIA 0
	${EndIf}
FunctionEnd

;安装PIA
Function PIAInstall

FunctionEnd
;=========== 环境Check End ===========

;点击，打开协议链接
Function onClickAgreement
  ExecShell open ${LICENSE_URL} ;"http://www.chinascopefinancial.com/website/privacy_policy"
FunctionEnd

;协议Checkbox点击效果 切换图片
Function onClick_CheckAgree
	${IF} $Bool_ShortCut == 1
		IntOp $Bool_ShortCut $Bool_ShortCut - 1
		StrCpy $1 $Ck_ShortCut
		Call SkinBtn_UnChecked
		EnableWindow $Btn_Next 0
	${ELSE}
		IntOp $Bool_ShortCut $Bool_ShortCut + 1
		StrCpy $1 $Ck_ShortCut
		Call SkinBtn_Checked
		EnableWindow $Btn_Next 1
	${EndIf}
FunctionEnd

Function onClickClose
		;MessageBox MB_ICONINFORMATION|MB_OK ${THIS_OUT_FILE}
    FindProcDLL::FindProc ${THIS_OUT_FILE}
    Sleep 500
    Pop $R0
    ${If} $R0 != 0
    	KillProcDLL::KillProc ${THIS_OUT_FILE}
    ${EndIf}
FunctionEnd

;下一步按钮事件
Function onClickNext
  StrCpy $R9 1 ;Goto the next page
  Call RelGotoPage
  Abort
FunctionEnd

;处理页面跳转的命令
Function RelGotoPage
  IntCmp $R9 0 0 Move Move
  StrCmp $R9 "X" 0 Move
  StrCpy $R9 "120"
  Move:
  SendMessage $HWNDPARENT "0x408" "$R9" ""
FunctionEnd

;首页安装按钮事件
Function onClickInstall
	;① 设置界面状态
	;② 关闭Office
	;③ 下载安装基础软件(nf,msi,vsto,pia等)
	;④ 安装主程序
	;   ① 下载主程序,解压至安装目录
	;   ② 设置目录权限everyone
	;   ③ 设置注册表vsto,xll
	;   ④ 注册URL协议
	;   ⑤ 配置安全证书
	Call InstallUIStatus

	Call CloseExcelApp

	Call DownloadDependencySoft

	Call DownloadMainProgram

	Call onCompeted
FunctionEnd

;安装的时候，界面设置
Function InstallUIStatus
  ShowWindow $Btn_Next ${SW_HIDE}
	ShowWindow $Btn_Waring ${SW_HIDE}
	;ShowWindow $Btn_Close ${SW_HIDE}
	ShowWindow $Ck_ShortCut ${SW_HIDE}
	ShowWindow $Lbl_ShortCut ${SW_HIDE}
	ShowWindow $Btn_Agreement ${SW_HIDE}
	ShowWindow $MiddleImage ${SW_HIDE}
	;EnableWindow $Btn_Close 0
	${NSW_SetWindowSize} $HWNDPARENT 525 200 ;改变窗体大小

	ShowWindow $Lbl_Sumary ${SW_SHOW}
	ShowWindow $PPercent ${SW_SHOW}
	ShowWindow $PB_ProgressBar ${SW_SHOW}
	;ShowWindow $WebImg ${SW_SHOW}
FunctionEnd

;下载安装依赖软件
Function DownloadDependencySoft
	;msi
	${If} $ISMSI == 0
	  MessageBox MB_OK "msi installing"
		Call MSIInstallation32
	${EndIf}

	;nf
	${If} $ISNETF == 0
	  MessageBox MB_OK "nf installing"
	  Call NF40Installation
	${EndIf}

	;vsto
	${If} $BOOL_VSTO == 0
	  MessageBox MB_OK "vsto installing"
	  Call VSTO40Install
	${EndIf}

	;pia
	${If} $BOOL_PIA == 0
	  MessageBox MB_OK "pia installing"
	  Call PIAInstall
	${EndIf}
FunctionEnd

;安装时下在主程序
Function DownloadMainProgram
  ${NSD_SetText} $Lbl_Sumary "正在下载主程序..."
	${NSD_SetText} $PPercent "0%"

	;IfFileExists $TEMP\CSFData.7z +12 0
	inetc::get /hwnd $PPercent /hwnd2 $PPercent /probar $PB_ProgressBar /caption " " /popup "" "http://update.otc-china.cn/updater/plugin/CSFData.zip" "$TEMP\CSFData.zip" /end
	Pop $0
	${If} $0 == "Transfer Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
	${ELSEIF} $0 == "SendRequest Error"
		MessageBox MB_ICONINFORMATION|MB_OK "下载出错"
		Call onClickClose
		Abort
  ${EndIf}
	System::Call "user32::InvalidateRect(i $hwndparent,i0,i 1)"
	Call InstallMainProgram

FunctionEnd

;安装主程序
Function InstallMainProgram
	${NSD_SetText} $Lbl_Sumary "正在安装主程序..."
	ShowWindow $PPercent ${SW_HIDE}

	SetOutPath $INSTDIR
	SetOverwrite on
	nsUnzip::Extract "$TEMP\CSFData.zip"

	;Nsis7z::Extract "$TEMP\CSFData.zip"

	;Delete "$TEMP\CSFData.zip"

	Call SetDirectoryFullControl

	Call InstallVstoRegister

	Call RegisterUrlProtocol

	Call MakeCert

FunctionEnd

;注册URL协议 csfdata://
Function RegisterUrlProtocol
	WriteRegExpandStr HKEY_CLASSES_ROOT ${THIS_URL_PROTOCOL_NAME} "" "URL:${THIS_URL_PROTOCOL_NAME} Protocol"
	WriteRegExpandStr HKEY_CLASSES_ROOT ${THIS_URL_PROTOCOL_NAME} "URL Protocol" ""
	WriteRegExpandStr HKEY_CURRENT_USER "${THIS_URL_PROTOCOL_NAME}\DefaultIcon" "" '${THIS_URL_PROTOCOL_EXE_COMMAND}'
	WriteRegExpandStr HKEY_CURRENT_USER "${THIS_URL_PROTOCOL_NAME}\shell\open\command" "" '${THIS_URL_PROTOCOL_EXE_COMMAND}'

	WriteRegExpandStr HKEY_CURRENT_USER "Software\Classes\${THIS_URL_PROTOCOL_NAME}" "" "URL:${THIS_URL_PROTOCOL_NAME} Protocol"
	WriteRegExpandStr HKEY_CURRENT_USER "Software\Classes\${THIS_URL_PROTOCOL_NAME}" "URL Protocol" ""
	WriteRegExpandStr HKEY_CURRENT_USER "Software\Classes\${THIS_URL_PROTOCOL_NAME}\DefaultIcon" "" '${THIS_URL_PROTOCOL_EXE_COMMAND}'
	WriteRegExpandStr HKEY_CURRENT_USER "Software\Classes\${THIS_URL_PROTOCOL_NAME}\shell\open\command" "" '${THIS_URL_PROTOCOL_EXE_COMMAND}'
FunctionEnd

;导入安全证书
Function MakeCert
	IfFileExists "$INSTDIR\ChinaScopeFinancial.cer" CertExists NotExists
	CertExists:
	  	IfFileExists "$INSTDIR\certMgr.exe" MakeExists NotExists
	MakeExists:
	    ;MessageBox MB_OK "aaa"
	    ExecCmd::exec '"$INSTDIR\certMgr.exe" -add "$INSTDIR\ChinaScopeFinancial.cer" -c -s -r localMachine TrustedPublisher'
	    ExecCmd::exec '"$INSTDIR\certMgr.exe" -add "$INSTDIR\ChinaScopeFinancial.cer" -c -s -r localMachine Root'
	NotExists:
FunctionEnd

;设置everyone full 权限
Function SetDirectoryFullControl
	ExecDos::exec '"$PLUGINSDIR\trust.bat" ${THIS_INSTALL_DIR}' "" ""
FunctionEnd

${StrStr}
Function RegisterVstoHKLM
    SetRegView 64
  	DeleteRegKey HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Delete"

  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "Description" "${INSTALL_AddInDesc}"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "FriendlyName" "${INSTALL_AddInDesc}"
  	WriteRegDWORD HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "LoadBehavior" "3"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "Manifest" "${THIS_INSTALL_DIR}\${INSTALL_VSTO}|vstolocal"

		WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\VSTO\Security\Inclusion\${INSTALL_VSTO_GUID}" "Url" "${THIS_INSTALL_DIR}\${INSTALL_VSTO}"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\VSTO\Security\Inclusion\${INSTALL_VSTO_GUID}" "PublicKey" "${INSTALL_PUBLICKEY}"

  	;watch
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "Description" "${INSTALL_AddInDesc}"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "FriendlyName" "${INSTALL_AddInDesc}"
  	WriteRegDWORD HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "LoadBehavior" "3"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "Manifest" "${THIS_INSTALL_DIR}\${INSTALL_VSTO}|vstolocal"

		WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\VSTO\Security\Inclusion\${INSTALL_WATCH_VSTO_GUID}" "Url" "${THIS_INSTALL_DIR}\${INSTALL_WATCH_VSTO}"
  	WriteRegExpandStr HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\VSTO\Security\Inclusion\${INSTALL_WATCH_VSTO_GUID}" "PublicKey" "${INSTALL_PUBLICKEY}"

  	;删除保护
  	WriteRegDWORD HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\$R9\Excel\Security\ProtectedView" "DisableInternetFilesInPV" "1"
  	WriteRegDWORD HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\$R9\Excel\Security\ProtectedView" "DisableAttachmentsInPV" "1"
  	WriteRegDWORD HKLM "Software\Microsoft\Office\$R9\User Settings\TADAExcelApp\Create\Software\Microsoft\Office\$R9\Excel\Security\ProtectedView" "DisableUnsafeLocationsInPV" "1"


  	;MAX_OPTION_OPEN
  	;注册XLL
  	;SetRegView 32
  	StrCpy $Bool_Xll_Installed 0
  	${ForEach} $i 0 ${MAX_OPTION_OPEN} + 1
  	  	${If} $i == 0
		  			StrCpy $open "OPEN"
				${Else}
		  			StrCpy $open "OPEN$i"
				${EndIf}

			  ReadRegStr $R1 HKCU "Software\Microsoft\Office\$R9\Excel\Options" $open
			  ${StrStr} $0  $R1 ${INSTALL_XLL}
			  ;MessageBox mb_ok "Reg:Software\Microsoft\Office\$R9\Excel\Options$\nopen:$open$\nvalue:$R1$\nFound:$0$\n${INSTALL_XLL}"
				StrCmp $0 "" NotFound
				StrCpy $Bool_Xll_Installed 1
				${ExitFor}
				;DeleteRegValue HKCU "Software\Microsoft\Office\$R9\Excel\Options" $open
				NotFound:
  	${Next}

  	${If} $Bool_Xll_Installed == 0
		  	${ForEach} $i 0 ${MAX_OPTION_OPEN} + 1
		  	  	${If} $i == 0
				  			StrCpy $open "OPEN"
						${Else}
				  			StrCpy $open "OPEN$i"
						${EndIf}

					  ReadRegStr $R1 HKCU "Software\Microsoft\Office\$R9\Excel\Options" $open
					  StrCmp $R1 "" NotInstall
					  ${Continue}
						NotInstall:
						  WriteRegStr HKCU "Software\Microsoft\Office\$R9\Excel\Options" $open '/R "${THIS_INSTALL_DIR}\${INSTALL_XLL}"'
						  ${ExitFor}
		  	${Next}
  	${EndIf}
FunctionEnd

;注册VSTO
Function InstallVstoRegister
	;---- Current User ----
  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "Description" "${INSTALL_AddInDesc}"
  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "FriendlyName" "${INSTALL_AddInDesc}"
  WriteRegDWORD HKCU "Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "LoadBehavior" "3"
  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\TADAExcelApp" "Manifest" "${THIS_INSTALL_DIR}\${INSTALL_VSTO}|vstolocal"

  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "Description" "${INSTALL_AddInDesc}"
  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "FriendlyName" "${INSTALL_AddInDesc}"
  WriteRegDWORD HKCU "Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "LoadBehavior" "3"
  WriteRegStr HKCU "Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp" "Manifest" "${THIS_INSTALL_DIR}\${INSTALL_WATCH_VSTO}|vstolocal"
  ;---- End ----

  /*
  StrCpy $0 "12.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2007
  ;StrCpy $Bool_Office2007 $1

  StrCpy $0 "14.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2010

  StrCpy $0 "15.0"
  Call CheckOfficeVersion
  Pop $Bool_Office2013
  */

  ;MessageBox MB_OK "2007:$Bool_Office2007$\n2010:$Bool_Office2010$\n2013:$Bool_Office2013$\n"

	;存在2007 版本
  ${IF} $Bool_Office2007 == 1
		StrCpy $R9 "12.0"
		Call RegisterVstoHKLM
	${EndIf}

	;存在2010
	${IF} $Bool_Office2010 == 1
		StrCpy $R9 "14.0"
		Call RegisterVstoHKLM
	${EndIf}

	;存在2013
	${IF} $Bool_Office2013 == 1
		StrCpy $R9 "15.0"
		Call RegisterVstoHKLM
	${EndIf}
FunctionEnd

;安装完成
Function onCompeted
  ;ShowWindow $HWNDPARENT ${SW_HIDE}
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayIcon" "${THIS_INSTALL_DIR}\chinascope.ico"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "InstallLocation" "${THIS_INSTALL_DIR}"

	WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "CSFData" "${THIS_INSTALL_DIR}"
  Call onClickNext
FunctionEnd

;取消按钮
Function onCancel
	Call onClickClose
FunctionEnd

Function WelcomePage
		Call CheckEnvironments

    GetDlgItem $0 $HWNDPARENT 1
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 2
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 3
    ShowWindow $0 ${SW_HIDE}

    nsDialogs::Create 1044
    Pop $0
    ${If} $0 == error
        Abort
    ${EndIf}
    SetCtlColors $0 ""  transparent ;背景设成透明

    ${NSW_SetWindowSize} $HWNDPARENT 525 300 ;改变窗体大小
    ${NSW_SetWindowSize} $0 525 300 ;改变Page大小

    ;安装/继续按钮
    ${NSD_CreateButton} 192 220 140 40 ""
		Pop $Btn_Next
		StrCpy $1 $Btn_Next
		Call SkinBtn_Next
		GetFunctionAddress $3 onClickInstall
    SkinBtn::onClick $1 $3

    ${If} $Bool_Office_Running == 1
      ${NSD_CreateButton} 160 273 198 15 ""
    	Pop $Btn_Waring
    	StrCpy $1 $Btn_Waring
    	Call SkinBtn_Waring
    ${EndIf}

    ;关闭按钮
  	${NSD_CreateButton} 494 1 30 30 ""
		Pop $Btn_Close
		StrCpy $1 $Btn_Close
		Call SkinBtn_Close
  	GetFunctionAddress $3 onClickClose ;onInstallCancel
  	SkinBtn::onClick $1 $3

    ;用户协议
	  ${NSD_CreateButton} 15 275 15 15 ""
	  Pop $Ck_ShortCut
	  StrCpy $1 $Ck_ShortCut
	  Call SkinBtn_Checked
	  GetFunctionAddress $3 onClick_CheckAgree
    SkinBtn::onClick $1 $3
	  StrCpy $Bool_ShortCut 1

    ${NSD_CreateLabel} 36 275 28 15 "接受"
    Pop $Lbl_ShortCut
    SetCtlColors $Lbl_ShortCut ""  transparent ;背景设成透明

    ;${CreateLinker2} "访问我的主页" 108 274 80 20 $HWNDPARENT ${IDC_LINKER} "http://hi.baidu.com/xstar2008"


		${NSD_CreateButton} 61 274 48 15 ""
		Pop $Btn_Agreement
		StrCpy $1 $Btn_Agreement
		Call SkinBtn_Agreement
	  GetFunctionAddress $3 onClickAgreement
	  SkinBtn::onClick $1 $3
 		StrCpy $Bool_License 0 ;初始化值为0

	 	;---------------------- 下载进度条
 		/*
 		System::Call `*(i,i,i,i)i(1,1,525,200).R0`
    System::Call `user32::MapDialogRect(i$HWNDPARENT,iR0)`
    System::Call `*$R0(i.s,i.s,i.s,i.s)`
    System::Free $R0
    FindWindow $R0 "#32770" "" $HWNDPARENT
    System::Call `user32::CreateWindowEx(i,t"STATIC",in,i${DEFAULT_STYLES}|${SS_BLACKRECT},i1,i1,i525,i200,iR0,i1100,in,in)i.R0`
    StrCpy $WebImg $R0
    WebCtrl::ShowWebInCtrl $WebImg "$PLUGINSDIR/index.htm"
    ShowWindow $WebImg ${SW_HIDE}
    GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $WebImg $0 ;处理无边框窗体移动
 		*/

 		${NSD_CreateLabel} 0 200 100 15 "正在安装..."
    Pop $Lbl_Sumary
    SetCtlColors $Lbl_Sumary ""  0xffffff ;背景设成透明
    ShowWindow $Lbl_Sumary ${SW_HIDE}
    GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $Lbl_Sumary $0 ;处理无边框窗体移动

    ;创建简要说明
    ${NSD_CreateLabel} 480 210 40 15 "10%"
    Pop $PPercent
    SetCtlColors $PPercent ""  0xffffff
	  ShowWindow $PPercent ${SW_HIDE}
	  GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $PPercent $0 ;处理无边框窗体移动

    ${NSD_CreateProgressBar} 0 195 525 4 ""
    Pop $PB_ProgressBar
    SkinProgress::Set $PB_ProgressBar "$PLUGINSDIR\loading2.bmp" "$PLUGINSDIR\loading1.bmp"
    ShowWindow $PB_ProgressBar ${SW_HIDE}
    GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $PB_ProgressBar $0 ;处理无边框窗体移动

		;贴背景大图
    ${NSD_CreateBitmap} 0 0 100% 100% ""
    Pop $BGImage
    ${NSD_SetImage} $BGImage $PLUGINSDIR\bg.bmp $ImageHandle
		GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $BGImage $0 ;处理无边框窗体移动
		${NSD_FreeImage} $ImageHandle

		;贴下部分
    ${NSD_CreateBitmap} 0 200 100% 100% ""
    Pop $MiddleImage
    ${NSD_SetImage} $MiddleImage $PLUGINSDIR\bg_buttom.bmp $ImageHandle
    GetFunctionAddress $0 onGUICallback
		WndProc::onCallback $MiddleImage $0 ;处理无边框窗体移动
		${NSD_FreeImage} $ImageHandle

		${NSD_SetFocus} $Btn_Next
    
		nsDialogs::Show
FunctionEnd

;完成界面
Function CompletePage
  GetDlgItem $0 $HWNDPARENT 1
  ShowWindow $0 ${SW_HIDE}
  GetDlgItem $0 $HWNDPARENT 2
  ShowWindow $0 ${SW_HIDE}
  GetDlgItem $0 $HWNDPARENT 3
  ShowWindow $0 ${SW_HIDE}

  nsDialogs::Create 1044
  Pop $0
	${If} $0 == error
		Abort
	${EndIf}
	SetCtlColors $0 ""  transparent ;背景设成透明

  ${NSW_SetWindowSize} $HWNDPARENT 520 200 ;改变自定义窗体大小
	${NSW_SetWindowSize} $0 520 200 ;改变自定义Page大小

	;关闭按钮
	${NSD_CreateButton} 494 1 30 30 ""
	Pop $Btn_Close
	StrCpy $1 $Btn_Close
	Call SkinBtn_Close
  GetFunctionAddress $3 onClickClose ;onInstallCancel
  SkinBtn::onClick $1 $3

	${NSD_CreateBitmap} 0 0 100% 100% ""
  Pop $BGImage
  ${NSD_SetImage} $BGImage $PLUGINSDIR\success.bmp $ImageHandle

	GetFunctionAddress $0 onGUICallback
  WndProc::onCallback $BGImage $0 ;处理无边框窗体移动
  nsDialogs::Show
  ${NSD_FreeImage} $ImageHandle

FunctionEnd

Function onWarningGUICallback
  ${If} $MSG = ${WM_LBUTTONDOWN}
    SendMessage $WarningForm ${WM_NCLBUTTONDOWN} ${HTCAPTION} $0
  ${EndIf}
FunctionEnd

;安装过程中，关闭
Function onInstallCancel
	IsWindow $WarningForm Create_End
	!define Style ${WS_VISIBLE}|${WS_OVERLAPPEDWINDOW}
	${NSW_CreateWindowEx} $WarningForm $hwndparent ${ExStyle} ${Style} "" 1018

	${NSW_SetWindowSize} $WarningForm 349 184
	EnableWindow $hwndparent 0
	System::Call `user32::SetWindowLong(i$WarningForm,i${GWL_STYLE},0x9480084C)i.R0`
	${NSW_CreateButton} 148 122 88 25 ''
	Pop $R0
	StrCpy $1 $R0
	Call SkinBtn_Quit
	${NSW_OnClick} $R0 onClickClose

	${NSW_CreateButton} 248 122 88 25 ''
	Pop $R0
	StrCpy $1 $R0
	Call SkinBtn_Cancel
	${NSW_OnClick} $R0 OnClickQuitCancel

	${NSW_CreateBitmap} 0 0 100% 100% ""
  Pop $BGImage
  ${NSW_SetImage} $BGImage $PLUGINSDIR\quit.bmp $ImageHandle
	GetFunctionAddress $0 onWarningGUICallback
	WndProc::onCallback $BGImage $0 ;处理无边框窗体移动
  ${NSW_CenterWindow} $WarningForm $hwndparent
	${NSW_Show}

	Create_End:
  	ShowWindow $WarningForm ${SW_SHOW}
FunctionEnd

Function OnClickQuitCancel
  ${NSW_DestroyWindow} $WarningForm
  EnableWindow $hwndparent 1
  BringToFront
FunctionEnd

Function onUserAbort
	call onInstallCancel
FunctionEnd

#=================== 卸载 =======================================================
Function un.onInit
    InitPluginsDir
    File `/ONAME=$PLUGINSDIR\un_bg.bmp` `images\un_bg.bmp`
    File `/ONAME=$PLUGINSDIR\bg_buttom.bmp` `images\bg_buttom.bmp`
    File `/ONAME=$PLUGINSDIR\success.bmp` `images\success.bmp`
    File `/ONAME=$PLUGINSDIR\btn_uninst.bmp` `images\btn_uninst.bmp`
    File `/oname=$PLUGINSDIR\btn_close.bmp` `images\btn_close.bmp`
    File `/oname=$PLUGINSDIR\waring.bmp` `images\waring.bmp`

		SkinBtn::Init "$PLUGINSDIR\btn_uninst.bmp"
    SkinBtn::Init "$PLUGINSDIR\btn_close.bmp"
    SkinBtn::Init "$PLUGINSDIR\waring.bmp"

FunctionEnd

Function un.onGUIInit
  ;消除边框
    System::Call `user32::SetWindowLong(i$HWNDPARENT,i${GWL_STYLE},0x9480084C)i.R0`
    ;隐藏一些既有控件
    GetDlgItem $0 $HWNDPARENT 1034
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1035
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1036
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1037
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1038
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1039
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1256
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 1028
    ShowWindow $0 ${SW_HIDE}
FunctionEnd

Function un.onGUICallback
  ${If} $MSG = ${WM_LBUTTONDOWN}
    SendMessage $HWNDPARENT ${WM_NCLBUTTONDOWN} ${HTCAPTION} $0
  ${EndIf}
FunctionEnd

;点击右上角关闭按钮
Function un.onClickClose
		SendMessage $HwndParent ${WM_CLOSE} 0 0
FunctionEnd

Function un.WelcomePage
    GetDlgItem $0 $HWNDPARENT 1
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 2
    ShowWindow $0 ${SW_HIDE}
    GetDlgItem $0 $HWNDPARENT 3
    ShowWindow $0 ${SW_HIDE}

    nsDialogs::Create 1044
    Pop $0
    ${If} $0 == error
        Abort
    ${EndIf}
    SetCtlColors $0 ""  transparent ;背景设成透明

    ${NSW_SetWindowSize} $HWNDPARENT 525 300 ;改变窗体大小
    ${NSW_SetWindowSize} $0 525 300 ;改变Page大小
    
    FindProcDLL::FindProc ${EXCEL_APP}
		Sleep 500
		Pop $R0
		${If} $R0 != 0
		  ;安装/继续按钮
	    ${NSD_CreateButton} 192 220 140 40 ""
			Pop $Btn_Next
			StrCpy $1 $Btn_Next
			Call un.SkinBtn_Next
			GetFunctionAddress $3 un.onClickUnistall
	    SkinBtn::onClick $1 $3
    
   		${NSD_CreateButton} 160 270 198 15 ""
    	Pop $Btn_Waring
    	StrCpy $1 $Btn_Waring
    	Call un.SkinBtn_Waring
		${Else}
		  ;安装/继续按钮
	    ${NSD_CreateButton} 192 230 140 40 ""
			Pop $Btn_Next
			StrCpy $1 $Btn_Next
			Call un.SkinBtn_Next
			GetFunctionAddress $3 un.onClickUnistall
	    SkinBtn::onClick $1 $3
		${EndIf}

    ;关闭按钮
  	${NSD_CreateButton} 494 1 30 30 ""
		Pop $Btn_Close
		StrCpy $1 $Btn_Close
		Call un.SkinBtn_Close
  	GetFunctionAddress $3 un.onClickClose ;onInstallCancel
  	SkinBtn::onClick $1 $3
    
		;贴背景大图
    ${NSD_CreateBitmap} 0 0 100% 100% ""
    Pop $BGImage
    ${NSD_SetImage} $BGImage $PLUGINSDIR\un_bg.bmp $ImageHandle
		GetFunctionAddress $0 un.onGUICallback
		WndProc::onCallback $BGImage $0 ;处理无边框窗体移动
		;${NSD_FreeImage} $ImageHandle

		;贴下部分
    ${NSD_CreateBitmap} 0 200 100% 100% ""
    Pop $MiddleImage
    ${NSD_SetImage} $MiddleImage $PLUGINSDIR\bg_buttom.bmp $ImageHandle
    GetFunctionAddress $0 un.onGUICallback
		WndProc::onCallback $MiddleImage $0 ;处理无边框窗体移动
		${NSD_FreeImage} $ImageHandle

		nsDialogs::Show
FunctionEnd

Function un.closeExcelApp
  FindProcDLL::FindProc ${EXCEL_APP}
	Pop $R0
	StrCmp $R0 1 0  +3
	KillProcDLL::KillProc ${EXCEL_APP}
	Goto -4
FunctionEnd

Function un.onClickUnistall
	;改变窗体状态
	;删除vst xll
	;删除url协议
	;获取安装目录，并删除
	;删除卸载信息
	
	ShowWindow $Btn_Next ${SW_HIDE}
	ShowWindow $Btn_Waring ${SW_HIDE}
	ShowWindow $MiddleImage ${SW_HIDE}
	${NSW_SetWindowSize} $HWNDPARENT 525 200 ;改变窗体大小
	
	Call un.closeExcelApp
	
	DeleteRegKey HKEY_CLASSES_ROOT ${THIS_URL_PROTOCOL_NAME}
	DeleteRegKey HKEY_CURRENT_USER "Software\Classes\${THIS_URL_PROTOCOL_NAME}"
  DeleteRegKey HKCU "Software\Microsoft\Office\Excel\Addins\TADAExcelApp"
  DeleteRegKey HKCU "Software\Microsoft\Office\Excel\Addins\ADAExcelWatchApp"
	
	StrCpy $R1 "12.0"
	Call un.uninstallVSTO
	
	StrCpy $R1 "14.0"
	Call un.uninstallVSTO
	
	StrCpy $R1 "15.0"
	Call un.uninstallVSTO
	
	ReadRegStr $R1 HKEY_LOCAL_MACHINE ${PRODUCT_UNINST_KEY} "InstallLocation"
	${If} $R1 != ""
	  Delete "$R1\uninst.exe"
  	Delete "$R1\*.*"
  	RMDir /r "$R1"
	${Else}
	  Delete "$INSTDIR\uninst.exe"
  	Delete "$INSTDIR\*.*"
  	RMDir /r "$INSTDIR"
	${EndIf}
 	DeleteRegKey HKLM "${PRODUCT_UNINST_KEY}"
 	
 	;${NSD_SetImage} $BGImage $PLUGINSDIR\success.bmp $ImageHandle
  ;${NSD_SetImage} $BGImage $PLUGINSDIR\success.bmp $ImageHandle
  ;ShowWindow $BGImage ${SW_SHOW}
	 ;Call un.onClickNext
	 Sleep 3000
	 SendMessage $HwndParent ${WM_CLOSE} 0 0
FunctionEnd

Function un.RelGotoPage
  IntCmp $R9 0 0 Move Move
  StrCmp $R9 "X" 0 Move
  StrCpy $R9 "120"
  Move:
  SendMessage $HWNDPARENT "0x408" "$R9" ""
FunctionEnd

;下一步按钮事件
Function un.onClickNext
  StrCpy $R9 1 ;Goto the next page
  Call un.RelGotoPage
  Abort
FunctionEnd

Function un.uninstallVSTO
	SetRegView 64
  DeleteRegKey HKLM "Software\Microsoft\Office\$R1\User Settings\TADAExcelApp"
  
	${ForEach} $i 0 ${MAX_OPTION_OPEN} + 1
  	${If} $i == 0
  			StrCpy $open "OPEN"
		${Else}
  			StrCpy $open "OPEN$i"
		${EndIf}

	  ReadRegStr $R2 HKCU "Software\Microsoft\Office\$R1\Excel\Options" $open
		StrLen $0 $R2
		StrLen $1 ${INSTALL_XLL}
		IntCmp $0 $1 NotFound NotFound
		IntOp $0 $0 - $1
		IntOp $0 $0 - 1
		StrCpy $2 $R2 $1 $0
		;MessageBox MB_OK "[$R1][$2]"
		${If} $2 == ${INSTALL_XLL}
				DeleteRegValue HKCU "Software\Microsoft\Office\$R1\Excel\Options" $open
				${ExitFor}
		${Else}
		    Goto NotFound
		${EndIf}
		NotFound:
	${Next}
FunctionEnd

Function un.CompletePage
	MessageBox MB_OK "un.CompletePage"
	
	GetDlgItem $0 $HWNDPARENT 1
  ShowWindow $0 ${SW_HIDE}
  GetDlgItem $0 $HWNDPARENT 2
  ShowWindow $0 ${SW_HIDE}
  GetDlgItem $0 $HWNDPARENT 3
  ShowWindow $0 ${SW_HIDE}

  nsDialogs::Create 1018
  Pop $0
	${If} $0 == error
	  MessageBox MB_OK "error"
		Abort
	${EndIf}
	MessageBox MB_OK "un.CompletePage2"
	SetCtlColors $0 ""  transparent ;背景设成透明

  ${NSW_SetWindowSize} $HWNDPARENT 520 200 ;改变自定义窗体大小
	${NSW_SetWindowSize} $0 520 200 ;改变自定义Page大小
  MessageBox MB_OK "un.CompletePage3"
	;关闭按钮
	${NSD_CreateButton} 494 1 30 30 ""
	Pop $Btn_Close
	StrCpy $1 $Btn_Close
	Call un.SkinBtn_Close
  GetFunctionAddress $3 un.onClickClose ;onInstallCancel
  SkinBtn::onClick $1 $3

	${NSD_CreateBitmap} 0 0 100% 100% ""
  Pop $BGImage
  ${NSD_SetImage} $BGImage $PLUGINSDIR\success.bmp $ImageHandle

	;GetFunctionAddress $0 un.onGUICallback
  ;WndProc::onCallback $BGImage $0 ;处理无边框窗体移动
  nsDialogs::Show
  ;${NSD_FreeImage} $ImageHandle
FunctionEnd
